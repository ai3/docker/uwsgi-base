#!/bin/sh

target=http://localhost:8080

if ! curl -sf -o /dev/null "$target" ; then
    echo "Could not connect to UWSGI test app"
    exit 1
fi

data=$(curl -sf "$target")
case "$data" in
    ok)
        echo "Successfully connected to the test Flask application at $target"
        exit 0
        ;;
    *)
        echo "Invalid response from test Flask application:"
        echo "$data"
        ;;
esac

exit 2
