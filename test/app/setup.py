from setuptools import setup, find_packages

setup(
	name='testapp',
	version='0.1',
	description='Test Flask app',
	packages=find_packages(),
	install_requires=[
          'cryptography',
	  'Flask',
	],
)
