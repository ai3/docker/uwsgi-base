from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
    return 'ok'


@app.route('/env')
def env():
    return ''.join(
        [f'{key}={value}\n' for key, value in request.environ.items()])
