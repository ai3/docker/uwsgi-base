#!/bin/sh

target=https://localhost:8443
sslopts="--cert ./test/pki/credentials/client/cert.pem
         --key ./test/pki/credentials/client/private_key.pem
         --cacert ./test/pki/credentials/ca.pem"
sslopts_other="--cert ./test/pki/ca/other.pem
         --key ./test/pki/ca/other.key
         --cacert ./test/pki/credentials/ca.pem"

if ! curl $sslopts -sf -o /dev/null "$target" ; then
    echo "Could not connect to UWSGI test app"
    exit 1
fi

data=$(curl $sslopts -sf "$target")
case "$data" in
    ok)
        echo "Successfully connected to the test Flask application at $target"
        ;;
    *)
        echo "Invalid response from test Flask application:"
        echo "$data"
        exit 2
        ;;
esac

# Attempt a connection with the 'other' cert, should fail TLS auth ACLs.
curl $sslopts_other -sf -o /dev/null "$target"
if [ $? -eq 0 ]; then
   echo "TLS auth failed (connected successfully with invalid CN)"
   exit 2
fi
echo "Successfully tested mTLS ACL deny"

exit 0
