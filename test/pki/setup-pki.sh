#!/bin/sh

# Setup the PKI for the CI SSL test.

script_dir=$(dirname "$0")
ca_dir="${script_dir}/ca"
creds_dir="${script_dir}/credentials"

set -e

${script_dir}/ca.sh ${ca_dir} ca
${script_dir}/ca.sh ${ca_dir} cert localhost
${script_dir}/ca.sh ${ca_dir} cert other

mkdir -p ${creds_dir}/client ${creds_dir}/server
cp ${ca_dir}/ca.pem ${creds_dir}/ca.pem
cp ${ca_dir}/localhost.pem ${creds_dir}/client/cert.pem
cp ${ca_dir}/localhost.key ${creds_dir}/client/private_key.pem
cp ${ca_dir}/localhost.pem ${creds_dir}/server/cert.pem
cp ${ca_dir}/localhost.key ${creds_dir}/server/private_key.pem
chmod -R a+r ${creds_dir}

exit 0
