#!/bin/bash
#
# Generate a test CA and associated client/server certs
#

bits=2048

usage() {
    echo "Usage: $0 <dir> <cmd>" >&2
    exit 2
}    

alt_names() {
    local out=
    for name in "$@"; do
        out="${out}${out:+,}DNS:${name}"
    done
    echo $out
}

ca_dir="$1"
[ -z "$ca_dir" ] && usage
[ -d "$ca_dir" ] || mkdir -p "$ca_dir"

ca_cert="${ca_dir}/ca.pem"
ca_key="${ca_dir}/ca.key"

set -e

cmd="$2"
shift 2

case "$cmd" in
    ca|"")
        [ -e "$ca_key" ] || openssl genrsa -out "$ca_key" "$bits"
        [ -e "$ca_cert" ] || \
            openssl req -x509 -new -nodes -key "$ca_key" -sha256 -days 1024 -out "$ca_cert" \
                    -subj "/C=XX/O=Test Org/CN=Test Org CA/"
        ;;

    cert)
        [ $# -lt 1 ] && usage
        cert_name="$1"
        
        cert="${ca_dir}/${cert_name}.pem"
        cert_key="${ca_dir}/${cert_name}.key"
        csr="${ca_dir}/${cert_name}.csr"
        cert_alt_names_str=$(alt_names "$@")

        openssl genrsa -out "$cert_key" "$bits"
        openssl req -new -sha256 \
                -key "${cert_key}" \
                -subj "/C=XX/O=Test Org/CN=${cert_name}" \
                -reqexts SAN \
                -config <(cat /etc/ssl/openssl.cnf && \
                              printf "\n[SAN]\nsubjectAltName=${cert_alt_names_str}") \
                -out "$csr"
        openssl x509 -req -in "$csr" -CA "$ca_cert" -CAkey "$ca_key" -CAcreateserial \
                -extfile <(printf "subjectAltName=${cert_alt_names_str}") \
                -out "$cert" -days 1024 -sha256
        rm -f "$csr"
        ;;
esac

exit 0
