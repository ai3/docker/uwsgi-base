import re
import os


def _abort(start_response, code_str):
    start_response(code_str, [('Content-Type', 'text/plain')])
    return [code_str.encode('utf-8')]


def _compile_config(config):
    # Turn config dictionaries into path/cn compiled regexp tuples.
    def _compile_entry(e):
        return (re.compile(e['path']), re.compile(e['cn']))
    return [_compile_entry(x) for x in config]


def _get_subject_cn(environ):
    # Return the subject CN of the client certificate, if present.
    # We are going to trust uwsgi's extraction of the client
    # certificate's subject into the HTTPS_DN variable.
    dn = environ.get('HTTPS_DN')
    if not dn:
        return None
    cnl = [x for x in dn.split('/') if x.startswith('CN=')]
    if len(cnl) != 1:
        return None
    return cnl[0][3:]


class TLSAuthMiddleware(object):

    def __init__(self, next_app, config):
        self._next_app = next_app
        self._config = _compile_config(config)

    def __call__(self, environ, start_response):
        path = environ['SCRIPT_NAME'] + environ['PATH_INFO']
        client_cn = _get_subject_cn(environ)
        if not client_cn:
            return _abort(start_response, '401 Unauthorized')
        for path_rx, cn_rx in self._config:
            if path_rx.match(path) and cn_rx.match(client_cn):
                return self._next_app(environ, start_response)
        return _abort(start_response, '403 Forbidden')


def tls_auth_wrap(next_app):
    """Wrap a WSGI application with TLSAuthMiddleware.

    Configuration is loaded from the file specified by the environment
    variable TLS_AUTH_CONFIG. If the variable is not defined or empty,
    TLS authentication is disabled.

    The configuration file should be a Python file defining a single
    ACL variable, a list of {path, cn} dictionaries containing ACL
    definitions.

    """
    config_file = os.getenv('TLS_AUTH_CONFIG')
    if not config_file:
        return next_app

    config_ctx = {'ACL': []}
    exec(open(config_file).read(), config_ctx)
    config = config_ctx['ACL']
    return TLSAuthMiddleware(next_app, config)
