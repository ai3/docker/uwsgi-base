uwsgi-base
===

Base image to run Python web applications using
[uwsgi](https://uwsgi-docs.readthedocs.io/). The image is based on
[s6-overlay-lite](https://git.autistici.org/ai3/docker/s6-overlay-lite),
to make it easy to add further processes (daemons, cron jobs, etc). It
supports mTLS authentication (with subject-based ACLs) in a somewhat
standardized fashion.

The resulting image makes as few assumptions on the running
environment as possible, it can run as an arbitrary non-root user with
a read-only container filesystem, and it tries to match modern best
practices for Python app deployment.

## Virtualenv

The image includes a virtualenv (in the */virtualenv* directory) ready
to use, which you can optionally take advantage of for installing your
app.

Its usage is triggered by the *VIRTUALENV* environment variable, which
should be defined and point at the desired virtualenv root directory.

The default virtualenv, already created in the base image, also
includes the Python modules which are necessary to support mTLS
authentication: so, if you are using this feature, using a new,
different, virtualenv will likely not work.

## Usage (build)

To use *uwsgi-base* to deploy your app, use this image as the base
layer of your application's image.

For Python apps, it is usually best to adopt a multi-stage build
approach, to avoid polluting the final image with build artifacts. A
good example is the embedded test Flask application located in
*test/app* in this same repository.

Let's examine its Dockerfile. First the application, and all its
dependencies, are built and packaged as *wheels* (a pre-built binary
package format for Python modules and libraries):

```
FROM debian:bookworm-slim AS build

# Install a Python development environment.
RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends \
        python3 python3-dev python3-pip python3-setuptools python3-wheel \
        git build-essential

# Build the source and package it (and its dependencies, as listed in
# requirements.txt) as Python wheels.
ADD . /src
WORKDIR /src
RUN mkdir -p dist && \
    pip3 wheel -r requirements.txt -w dist && \
    python3 setup.py bdist_wheel
```

Now the /src/dist directory will contain a bunch of *.whl* files
containing the packaged application and all of its dependencies, which
we are going to use in the final stage:

```
FROM registry.git.autistici.org/ai3/docker/uwsgi-base:master

COPY --from=build /src/dist/*.whl /tmp/wheels/
RUN cd /tmp/wheels \
    && /virtualenv/bin/pip3 install *.whl \
    && rm -fr /tmp/wheels
```

All that is done here is to install the packages into the
application's virtualenv.

Finally we have to tell *uwsgi-base* to use the image virtualenv when
loading the application:

```
ENV VIRTUALENV=/virtualenv
```

## Usage (runtime)

The runtime behavior of the uwsgi base image can be controlled via
environment variables. You must at least define *BIND_ADDR* and
*WSGI_APP* for the container to run, there are no defaults.

* `BIND_ADDR` is the address that the uwsgi daemon should listen on,
  in *host:port* syntax (or just *:port*, with leading colon, to
  listen on all addresses).
* `WSGI_APP` defines the application WSGI entry point, usually with
  the syntax *path.to.Python.module*:*callable*.
* `VIRTUALENV` (optional) if set, use the Python virtualenv at the
  given path to load the application.

The following options control TLS listeners and mTLS authentication.
They are meant to simplify integration with
[float](https://git.autistici.org/ai3/float). They are all optional.

* `SERVICE_CREDENTIALS` enables HTTPS when set, and it is the name of
  the float service credentials to use. The certificate and private
  key are expected to be found below
  */etc/credentials/x509/SERVICE_CREDENTIALS/server*.
* `TLS_AUTH_CONFIG` enables mTLS authentication. If set, it should
  point at a Python file defining the ACL rules to use (see below for
  the exact syntax).

Finally there are a few variables that directly control
resource-related parameters of *uwsgi* itself:

* `UWSGI_WORKERS` (default: 3) sets the number of uwsgi workers
* `UWSGI_PROCESSES` (default: 1) sets the number of uwsgi processes
* `UWSGI_ASYNC_CORES` (default: 100) controls gevent concurrency

### mTLS ACL syntax

The file pointed to by TLS_AUTH_CONFIG must be a Python file, defining
one top-level variable `ACL`, which contains a list of access-control
patterns. For each requests, at least one of these patterns must match
the request parameters. ACL patterns are made of two regular
expressions, one that should match the HTTP request path, another that
should match the client certificate's subject CN. They are expressed
as Python dictionaries with *path* and *cn* attributes, for instance:

```python
ACL = [
  {"path": "^/api/", "cn": "^other-service$"},
  {"path": "^/metrics$", "cn": ".*"},
]
```
