FROM registry.git.autistici.org/ai3/docker/s6-overlay-lite:master

COPY build.sh /tmp/build.sh
COPY utils /tmp/utils
COPY conf/ /etc/

RUN /tmp/build.sh && rm /tmp/build.sh
